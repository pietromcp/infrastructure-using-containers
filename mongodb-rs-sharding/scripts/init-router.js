use config

const chunkSize = 1
print("Setting 'chunkSize' to " + chunkSize + " Mb")
db.settings.insertOne( { _id:"chunksize", value: chunkSize } )

print("Add shard rs1")
sh.addShard("rs1/mongo10:27100,mongo11:27101,mongo12:27102")
print("Add shard rs2")
sh.addShard("rs2/mongo20:27200,mongo21:27201,mongo22:27202")
print("Add shard rs2")
sh.addShard("rs3/mongo30:27300,mongo31:27301,mongo32:27302")

use bigdb
print("Create sharding-supporting indexes")
db.bigcollx.createIndex({ x : 1 })
db.bigcollab.createIndex({ a : 1, b: 1 })
print("Enable sharding for database 'bigdb'")
sh.enableSharding("bigdb")
print("Shard collection 'bigdb.bigcollx'")
sh.shardCollection("bigdb.bigcollx", { x: 1 } )
print("Shard collection 'bigdb.bigcollab'")
sh.shardCollection("bigdb.bigcollab", { a: 1, b: 1 } )
print("Done")